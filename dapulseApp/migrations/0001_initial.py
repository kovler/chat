# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-05-04 22:55
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content', models.CharField(default=b'', max_length=200)),
                ('time', models.TimeField(default=b'00:00')),
                ('date', models.DateField(default=b'some')),
            ],
        ),
        migrations.CreateModel(
            name='UserName',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default=b'', max_length=50)),
            ],
        ),
        migrations.AddField(
            model_name='message',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='dapulseApp.UserName'),
        ),
    ]
