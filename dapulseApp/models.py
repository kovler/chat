from django.db import models
from django.utils.encoding import python_2_unicode_compatible
import time


@python_2_unicode_compatible
class Message(models.Model):
    name = models.CharField(default='', max_length=50)
    content = models.CharField(default='', max_length=200)
    time = models.IntegerField(default=0)

    def __str__(self):
        return self.name + "'s message"
