from django.conf.urls import url, include
from rest_framework import routers
import views


router = routers.DefaultRouter()
router.register(r'messages', views.MessageViewSet)

urlpatterns = [
    url(r'^accounts/', include('registration.backends.hmac.urls')),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    url(r'^send/', views.post_msg, name='post_msg'),
    url(r'^chat/(?P<name>[a-z]+)/', views.chat, name='chat')
]
