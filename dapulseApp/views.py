from rest_framework import viewsets
from django.shortcuts import render
from serializer import MessageSerializer
from .models import Message
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt


def chat(request, name):
    context = {"name": name}
    return render(request, './chat.html', context)


@csrf_exempt
def post_msg(request):
    name = request.POST["user"]
    content = request.POST["content"]
    time = int(request.POST["time"])
    new_msg = Message(name=name, content=content, time=time)
    new_msg.save()
    return render(request, './chat.html', {})


class MessageViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    def get_queryset(self):

        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `username` query parameter in the URL.
        """
        queryset = Message.objects.all()
        time = self.request.query_params.get('time', None)
        if time is not None:
            queryset = queryset.filter(time__gte=time)
        return queryset
